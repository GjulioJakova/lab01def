package main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import controller.Implementations.LoginControllerImpl;
import model.Implementations.CellImpl;
import model.Implementations.CellImplBuilder;
import model.Implementations.GuardImpl;
import model.Implementations.GuardImplBuilder;
import view.Interfaces.LoginView;

/**
 * The main of the application.
 */
public final class Main {

	private static int FIRST_FLOOR = 20;
	private static int SECOND_FLOOR = 40;
	private static int THIRD_FLOOR = 45;
	private static int TOTAL_FLOOR = 50;
	private static String PATH_DIRECTORY = "res";
	private static String PATH_CELLS = "res/Celle.txt";
	private static String PATH_GUARDS = "res/GuardieUserPass.txt";
	/**
     * Program main, this is the "root" of the application.
     * @param args
     * unused,ignore
     */
	 public static void main(final String... args){

		 new File(PATH_DIRECTORY).mkdir();
		 File fileGuards = new File(PATH_CELLS);
		 if(fileGuards.length()==0){
			 try {
				initializeGuards(fileGuards);
			} catch (IOException e) {
				e.printStackTrace();
			}
		 }

		 File fileCells = new File(PATH_GUARDS);
		 if(fileCells.length()==0){
			 try {
				initializeCells(fileCells);
			} catch (IOException e) {
				e.printStackTrace();
			}
		 }

		 new LoginControllerImpl(new LoginView());
	 }
	 
	 /**
	  * metodo che inizializza le celle
	  * @param fileCells file in cui creare le celle
	  * @throws IOException
	  */
	 static void initializeCells(File fileCells)throws IOException {

		 List<CellImpl> list = new ArrayList<>();
		 CellImpl cell;
		 for(int i=0;i<TOTAL_FLOOR;i++){
			 if(i<FIRST_FLOOR){
				  cell = new CellImplBuilder().setId(i).setPosition("Primo piano").setCapacity(4).createCellImpl();
			 }
			 else if(i<SECOND_FLOOR){
					  cell = new CellImplBuilder().setId(i).setPosition("Secondo piano").setCapacity(3).createCellImpl();
				 }
			 else if(i<THIRD_FLOOR){
					  cell = new CellImplBuilder().setId(i).setPosition("Terzo piano").setCapacity(4).createCellImpl();
				 }
			else{
			 	      cell = new CellImplBuilder().setId(i).setPosition("Piano sotterraneo, celle di isolamento").setCapacity(1).createCellImpl();
			    }
			 list.add(cell);
		 }

		 saveCellsInFile(list);

	 }

	 private static void saveCellsInFile(final Iterable<CellImpl> list) throws IOException{
		 FileOutputStream fileOutputStream = new FileOutputStream(PATH_CELLS);
		 ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
		 objectOutputStream.flush();
		 fileOutputStream.flush();

		 for(CellImpl c : list){
			 objectOutputStream.writeObject(c);
		 }
		 objectOutputStream.close();
	 }
	 /**
	  * metodo che inizializza le guardie
	  * @param fileGuards file in cui creare le guardie
	  * @throws IOException
	  */
	 static void initializeGuards(File fileGuards) throws IOException{

		 String pattern = "MM/dd/yyyy";
		 SimpleDateFormat format = new SimpleDateFormat(pattern);
		 Date date = null;
		try {
			date = format.parse("01/01/1980");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		List<GuardImpl> list = new ArrayList<>();
		GuardImpl g1 = new GuardImplBuilder().setName("Oronzo").setSurname("Cantani").setBirthDate(date).setRank(1).setTelephoneNumber("0764568").setIdGuardia(1).setPassword("ciao01").createGuardImpl();
		list.add(g1);
		GuardImpl g2 = new GuardImplBuilder().setName("Emile").setSurname("Heskey").setBirthDate(date).setRank(2).setTelephoneNumber("456789").setIdGuardia(2).setPassword("asdasd").createGuardImpl();
		list.add(g2);
		GuardImpl g3 = new GuardImplBuilder().setName("Gennaro").setSurname("Alfieri").setBirthDate(date).setRank(3).setTelephoneNumber("0764568").setIdGuardia(3).setPassword("qwerty").createGuardImpl();
		list.add(g3);

		saveGuardsInFile(list);
	 }

	private static void saveGuardsInFile(final Iterable<GuardImpl> list) throws IOException{
		FileOutputStream fileOutputStream = new FileOutputStream(PATH_GUARDS);
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
		objectOutputStream.flush();
		fileOutputStream.flush();

		for(GuardImpl guard : list){
			objectOutputStream.writeObject(guard);
		}
		objectOutputStream.close();
	}
}
